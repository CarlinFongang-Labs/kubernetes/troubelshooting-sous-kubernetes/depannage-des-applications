# Dépannage des applications dans Kubernetes

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

Bonjour, dans cette vidéo, nous allons parler du dépannage de vos applications dans Kubernetes. Voici un aperçu rapide de ce dont nous allons discuter :

- Vérification de l'état des pods
- Exécution de commandes à l'intérieur des conteneurs
- Démonstration pratique

## Vérification de l'état des pods

Vous pouvez voir l'état d'un pod simplement avec la commande `kubectl get pods`, que nous avons déjà utilisée tout au long de ce cours. Vous pouvez également utiliser `kubectl describe pod` pour obtenir plus d'informations sur ce qui peut se passer avec un pod en mauvaise santé. Voici la commande :

```shell
kubectl describe pod <nom_du_pod>
```

## Exécution de commandes à l'intérieur des conteneurs

Si vous avez besoin de dépanner ce qui se passe à l'intérieur d'un conteneur, vous pouvez utiliser `kubectl exec` pour exécuter des commandes à l'intérieur de ces conteneurs. Si un pod contient plusieurs conteneurs, vous pouvez utiliser `-c` pour spécifier le nom du conteneur, suivi de deux tirets `--`, puis de la commande que vous souhaitez exécuter.

```shell
$ kubectl exec podname -c containername -- command
```

 Cependant, notez que vous ne pouvez pas utiliser `kubectl exec` pour exécuter un logiciel qui n'est pas présent dans le conteneur lui-même. Si le logiciel dans votre conteneur est très limité, vous pouvez être très limité sur ce que vous pouvez réellement faire depuis ce conteneur.



## Démonstration pratique

Voyons maintenant une démonstration pratique de ces techniques de base pour dépanner nos applications. Nous sommes connectés à notre nœud de plan de contrôle Kubernetes, et nous allons créer un pod de base avec lequel travailler. Voici comment créer un fichier `troubleshooting-pod.yml` :

```bash
nano troubleshooting-pod.yml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: troubleshooting-pod
spec:
  containers:
  - name: busybox
    image: busybox
    command: ['sh', '-c', 'while true; do sleep 5; done']
```

Nous enregistrons et quittons le fichier, puis nous créons notre pod avec la commande `kubectl apply` :

```shell
kubectl apply -f troubleshooting-pod.yml
```

>![Alt text](img/image.png)
*Pod crée*

Ensuite, nous vérifions l'état du pod :

```shell
kubectl get pods
```

>![Alt text](img/image-1.png)
*Le pod est bien opérationnel*


Si l'état est `Running`, cela signifie que votre pod est en cours d'exécution. Si c'est `Completed`, cela signifie que votre pod s'est exécuté avec succès et n'est plus en cours d'exécution. Cela vous donne une idée de ce qui se passe avec votre pod. Si un échec ou une erreur est signalé, vous savez que quelque chose ne va pas.

Pour obtenir plus d'informations sur un pod, vous pouvez utiliser `kubectl describe pod` :

```shell
kubectl describe pod troubleshooting-pod
```

>![Alt text](img/image-2.png)
*Spécification du pod*

Cette commande vous donne plus d'informations sur votre pod, y compris les événements qui se sont produits et la spécification du pod.

Ensuite, voyons comment exécuter une commande à l'intérieur d'un pod. Nous utilisons `kubectl exec` :

```shell
kubectl exec troubleshooting-pod -- ls
```

>![Alt text](img/image-3.png)

Cependant, vous ne pouvez exécuter que des commandes pour les logiciels présents dans le conteneur. Par exemple, si vous essayez d'exécuter `curl google.com` et que l'image de votre conteneur BusyBox n'inclut pas `curl`, vous obtiendrez un message d'erreur indiquant que le fichier exécutable ne peut pas être trouvé dans le chemin de ce conteneur.

```shell
kubectl exec troubleshooting-pod -- curl google.com
```

>![Alt text](img/image-4.png)

La commande ne marche pas ici car curl n'est pas installé.


Pour obtenir un shell interactif dans le conteneur, vous pouvez utiliser les commandes suivantes :

```shell
kubectl exec troubleshooting-pod -c busybox --stdin --tty -- /bin/sh
```

Cette commande vous donne un shell interactif dans votre conteneur, vous permettant d'exécuter plusieurs commandes. Par exemple :

>![Alt text](img/image-5.png)



## Récapitulatif

Nous avons discuté de la vérification de l'état des pods avec `kubectl get` et `kubectl describe`. Nous avons parlé de l'exécution de commandes à l'intérieur des conteneurs, y compris comment obtenir un shell interactif dans notre conteneur. Ensuite, nous avons fait une démonstration pratique de ces concepts. C'est tout pour cette leçon. Nous vous retrouverons dans la prochaine leçon.



# Reférence

https://kubernetes.io/docs/tasks/debug/debug-application/

https://kubernetes.io/docs/tasks/debug/debug-application/get-shell-running-container/

https://kubernetes.io/docs/tasks/debug/debug-application/determine-reason-pod-failure/